package au.com.boundless_systems.example

import au.com.boundless_systems.example.defaults.AppConfig
import au.com.boundless_systems.example.defaults.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

open class ExampleApplication: DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .applicationContext(this)
            .appConfig(AppConfig())
            .build()
    }
}