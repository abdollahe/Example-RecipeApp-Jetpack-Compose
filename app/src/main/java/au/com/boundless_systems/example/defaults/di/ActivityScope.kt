package au.com.boundless_systems.example.defaults.di

import javax.inject.Scope

/**
 * Defines a dagger scope that only lasts for the life of an activity.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
