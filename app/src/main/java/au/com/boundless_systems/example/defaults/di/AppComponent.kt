package au.com.boundless_systems.example.defaults.di

import android.content.Context
import au.com.boundless_systems.example.ExampleApplication
import au.com.boundless_systems.example.defaults.AppConfig
import au.com.boundless_systems.example.defaults.di.viewmodel.ViewModelFactory
import au.com.boundless_systems.example.defaults.di.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    AndroidBindingModule::class,
    ShoppingModule::class,
    ViewModelModule::class
])
interface AppComponent : AndroidInjector<ExampleApplication> {

    fun provideViewModelFactory(): ViewModelFactory

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(context: Context): Builder

        @BindsInstance
        fun appConfig(appConfig: AppConfig): Builder

        fun build(): AppComponent
    }
}