package au.com.boundless_systems.example.defaults.di

import au.com.boundless_systems.example.defaults.AppConfig
import au.com.boundless_systems.example.repository.RecipeRepository
import au.com.boundless_systems.example.repository.RecipesRepositoryImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class ShoppingModule {

    @Provides
    @Singleton
    fun provideRecipesRepositoryImpl(
        appConfig: AppConfig,
        okHttpClient: OkHttpClient,
        gson: Gson
    ): RecipesRepositoryImpl {
        return RecipesRepositoryImpl(
            appConfig,
            okHttpClient,
            gson
        )
    }

    @Provides
    @Singleton
    fun providesRecipeRepository(recipeRepositoryImpl : RecipesRepositoryImpl) : RecipeRepository {
        return (recipeRepositoryImpl as RecipeRepository)
    }
}