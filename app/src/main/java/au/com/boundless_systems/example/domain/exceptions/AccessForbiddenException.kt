package au.com.boundless_systems.example.domain.exceptions

class AccessForbiddenException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_ACCESS_FORBIDDEN_ERROR , message)