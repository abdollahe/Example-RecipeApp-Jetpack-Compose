package au.com.boundless_systems.example.domain.exceptions

class BadRequestException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_BAD_REQUEST_ERROR , message)