package au.com.boundless_systems.example.domain.exceptions

class DataRetrievalException(message : String? = null) :
    DomainException(DomainExceptionCodes.DATA_RETRIEVAL_ERROR , message)