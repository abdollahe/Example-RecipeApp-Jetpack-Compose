package au.com.boundless_systems.example.domain.exceptions

abstract class DomainException(
    private val errorCode : Int,
    private val errorMessage: String?) : RuntimeException(errorMessage)