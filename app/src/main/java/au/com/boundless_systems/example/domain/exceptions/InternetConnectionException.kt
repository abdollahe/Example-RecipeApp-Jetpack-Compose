package au.com.boundless_systems.example.domain.exceptions

class InternetConnectionException(message : String? = null) :
    DomainException(DomainExceptionCodes.INTERNET_CONNECTION_ERROR , message)