package au.com.boundless_systems.example.domain.exceptions

class NotFoundException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP__NOT_FOUND_ERROR , message)