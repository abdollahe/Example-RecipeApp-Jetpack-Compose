package au.com.boundless_systems.example.domain.exceptions

class RecipeDetailsSearchException(message : String? = null) :
    DomainException(DomainExceptionCodes.RECIPE_DETAILS_FETCH_ERROR , message)