package au.com.boundless_systems.example.domain.exceptions

class UnauthorisedAccessException(message : String? = null) :
    DomainException(DomainExceptionCodes.HTTP_UNAUTHORIZED_ACCESS_ERROR , message)