package au.com.boundless_systems.example.domain.exceptions

class UnexpectedInternalException(message : String? = null) :
    DomainException(DomainExceptionCodes.UNEXPECTED_INTERNAL_ERROR , message)