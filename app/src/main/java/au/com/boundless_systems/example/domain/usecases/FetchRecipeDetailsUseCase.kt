package au.com.boundless_systems.example.domain.usecases


import android.content.res.Resources
import au.com.boundless_systems.example.R
import au.com.boundless_systems.example.data.api.RecipesItem
import au.com.boundless_systems.example.domain.exceptions.RecipeDetailsSearchException
import au.com.boundless_systems.example.presentation.recipedetails.RecipeItemPresentation
import au.com.boundless_systems.example.presentation.recipelist.RecipeDifficultyLevel
import au.com.boundless_systems.example.repository.RecipeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchRecipeDetailsUseCase @Inject constructor(
    private val recipeRepository: RecipeRepository,
    private val resources: Resources
) {

    suspend fun getRecipeDetails(defaultDispatcher : CoroutineDispatcher = Dispatchers.Default , recipeId : String) : RecipeItemPresentation {
        return try {
           withContext(defaultDispatcher) {
               recipeRepository.getRecipes().filter {
                   it.id == recipeId
               }.map {
                   mapRecipeToPresentation(it)
               }.first()
           }
        }
        catch(e : Exception) {
            throw RecipeDetailsSearchException(resources.getString(R.string.retrieve_recipe_details_error_message))
        }
    }

    private fun mapRecipeToPresentation(recipeItem : RecipesItem) : RecipeItemPresentation {
        return RecipeItemPresentation(
            recipeName = recipeItem.name ,
            recipeHeadline = recipeItem.headline,
            image = recipeItem.image,
            calories = recipeItem.calories,
            difficultyLevel = RecipeDifficultyLevel.fromInt(recipeItem.difficulty) ,
            description = recipeItem.description,
            proteins = recipeItem.proteins
        )
    }

}