package au.com.boundless_systems.example.presentation

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import au.com.boundless_systems.example.defaults.di.viewmodel.ViewModelFactory
import au.com.boundless_systems.example.presentation.navigation.Navigation
import au.com.boundless_systems.example.presentation.theme.RecipeAppTheme
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContent {
            RecipeAppTheme {
                val navController = rememberAnimatedNavController()
                Navigation(
                    navController = navController ,
                    factory = viewModelFactory
                )
            }
        }
    }
}