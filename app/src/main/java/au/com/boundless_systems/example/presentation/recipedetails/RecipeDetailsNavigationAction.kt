package au.com.boundless_systems.example.presentation.recipedetails

sealed class RecipeDetailsNavigationAction {
    object GoBackAction : RecipeDetailsNavigationAction()
    object CloseApplication : RecipeDetailsNavigationAction()
}