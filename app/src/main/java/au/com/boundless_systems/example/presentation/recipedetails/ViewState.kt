package au.com.boundless_systems.example.presentation.recipedetails

import au.com.boundless_systems.example.presentation.recipelist.RecipeDifficultyLevel

sealed class ViewState {
    object Loading : ViewState()
    data class Error(val errorMessage : String) : ViewState()
    data class Ready(
        val recipe : RecipeItemPresentation
    ) : ViewState()
}

data class RecipeItemPresentation(
    val recipeName : String,
    val recipeHeadline : String,
    val description : String,
    val image: String,
    val proteins: String,
    val difficultyLevel: RecipeDifficultyLevel,
    val calories: String
    )


