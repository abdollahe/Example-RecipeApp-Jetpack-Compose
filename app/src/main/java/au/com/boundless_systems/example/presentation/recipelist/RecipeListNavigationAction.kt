package au.com.boundless_systems.example.presentation.recipelist

sealed class RecipeListNavigationAction {
    data class ShowRecipeDetails(val selectedRecipeId : String) : RecipeListNavigationAction()
    object CloseApplication : RecipeListNavigationAction()
}