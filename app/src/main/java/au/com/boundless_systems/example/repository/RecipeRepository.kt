package au.com.boundless_systems.example.repository

import au.com.boundless_systems.example.data.api.RecipesItem
import kotlinx.coroutines.flow.Flow

interface RecipeRepository {
    fun getRecipes() : Flow<RecipesItem>
}