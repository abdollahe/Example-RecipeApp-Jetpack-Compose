package au.com.boundless_systems.example.repository.api

import au.com.boundless_systems.example.data.api.Recipes
import retrofit2.http.GET

interface RecipesApi {
    @GET("android-test/recipes.json")
    suspend fun getRecipes() : Recipes
}