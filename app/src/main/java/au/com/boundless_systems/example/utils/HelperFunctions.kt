package au.com.boundless_systems.example.utils

import android.content.Context
import android.content.ContextWrapper
import androidx.appcompat.app.AppCompatActivity
import au.com.boundless_systems.example.R
import au.com.boundless_systems.example.presentation.recipelist.RecipeDifficultyLevel

fun Context.getActivity(): AppCompatActivity? {
    var currentContext = this
    while (currentContext is ContextWrapper) {
        if (currentContext is AppCompatActivity) {
            return currentContext
        }
        currentContext = currentContext.baseContext
    }
    return null
}

fun Context.closeActivity() {
    getActivity()?.finish()
}

fun getDifficultyImageResource(item : RecipeDifficultyLevel) : Int {
    return when(item) {
        RecipeDifficultyLevel.EASY -> R.drawable.ic_level_0_hard_face
        RecipeDifficultyLevel.NORMAL -> R.drawable.ic_level_1_hard_face
        RecipeDifficultyLevel.MEDIUM -> R.drawable.ic_level_2_hard_face
        RecipeDifficultyLevel.HARD -> R.drawable.ic_level_3_hard_face
    }
}
