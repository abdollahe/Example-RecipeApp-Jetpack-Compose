

# Notes and Assumptions on development.
- The focus for the development was towards the interactions of the classes and how data and control flows amongst the application not on the UI design.
- The UI design for both screen is fairly basic with respect to UX and aesthetics but the compose based UI does demonstrate how recomposition is managed and how state is retrieved from the viewmodel for population of the screens.
- Some minor changes has been made to the Retrofit client to use Kotlin Flows instead of Rxjava for emission of the api responses.
- Git and version control and branching strategies have not been used during development given the time and scale of the test project.

# Recipe Example App 

Purpose of this codebase is to show Android development knowledge of Abdollah Ebadi in practice.
The example app covers the following principles:

- Dagger Injection
- Domain layer creation and interaction
- Propagation of data from the domain through to a view
- Use of MVVM
- Use of the Observable pattern

This example app does not represent design skills, as such, time and effort has not been put towards the UI design.


### Description

 A simple application that reads from a collection of recipes and displays them within a list on the main page.

The list of recipes can be found here: https://hf-android-app.s3-eu-west-1.amazonaws.com/android-test/recipes.json

Each item view in the list shows the following:
- Recipe Name
- Recipe Headline (as subtitle)
- Recipe Thumbnail
- Recipe Calories
- Recipe difficulty

Additionally to the following details are shown to the user when they click on a recipe item.
- Recipe Name
- Recipe Headline (as subtitle)
- Recipe description
- Recipe calories
- Recipe proteins
- Recipe Image (Optional)


Dependency injection is used to inject both the viewModel into the view, and the repository into the viewModel.

Kotlin flows has been used for the propagation of data through the layer of the app.




